﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorLogic
{
    public abstract class Mage : IActions
    {
        public string Name { get; set; }
        public RaceEnum Race { get; set; }
        public GenderEnum Gender { get; set; }
        public ClassEnum ClassName { get; set; }

        public Mage(string name, RaceEnum race, GenderEnum gender)
        {
            Name = name;
            Race = race;
            Gender = gender;
            ClassName = ClassEnum.Mage;
        }

        public abstract void Move();

        public abstract void Attack<T>(T enemy);

        public abstract void SpecialAttack<T>(T enemy);

    }
}
