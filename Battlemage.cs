﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorLogic
{
    public class Battlemage : Mage
    {
        public SubclassEnum SubclassName { get; set; }
        public double HP { get; set; }
        public int Mana { get; set; }
        public int ArmorRating { get; set; }
        public int MovementSpeed { get; set; }
        public int AttackDamage { get; set; }
        public int SpecialAttackDamage { get; set; }

        public Battlemage(string name, RaceEnum race, GenderEnum gender)
            : base(name, race, gender)
        {
            SubclassName = SubclassEnum.Battlemage;
            HP = 370;
            Mana = 300;
            ArmorRating = 15;
            MovementSpeed = 120;
            AttackDamage = 50;
            SpecialAttackDamage = 80;
        }

        public override void Move()
        {
            throw new NotImplementedException();
        }

        public override void Attack<T>(T enemy)
        {
            if ((Mana - 25) >= 0)
            {
                if (typeof(T) == typeof(Knight))
                {
                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {AttackDamage} dmg to {enemy}");
                    Mana = Mana - 25;

                    Knight enemyObj = (Knight)(object)enemy;
                    enemyObj.HP = enemyObj.HP - AttackDamage;

                }
                else if (typeof(T) == typeof(Defender))
                {
                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {AttackDamage} dmg to {enemy}");
                    Mana = Mana - 25;

                    Defender enemyObj = (Defender)(object)enemy;
                    enemyObj.HP = enemyObj.HP - AttackDamage;
                }
                else if (typeof(T) == typeof(Healer))
                {
                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {AttackDamage} dmg to {enemy}");
                    Mana = Mana - 25;

                    Healer enemyObj = (Healer)(object)enemy;
                    enemyObj.HP = enemyObj.HP - AttackDamage;
                }
                else if (typeof(T) == typeof(Battlemage))
                {
                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {AttackDamage} dmg to {enemy}");
                    Mana = Mana - 25;

                    Battlemage enemyObj = (Battlemage)(object)enemy;
                    enemyObj.HP = enemyObj.HP - AttackDamage;
                }
                else if (typeof(T) == typeof(Archer))
                {
                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {AttackDamage} dmg to {enemy}");
                    Mana = Mana - 25;

                    Archer enemyObj = (Archer)(object)enemy;
                    enemyObj.HP = enemyObj.HP - AttackDamage;
                }
                else if (typeof(T) == typeof(Assassin))
                {
                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {AttackDamage} dmg to {enemy}");
                    Mana = Mana - 25;

                    Assassin enemyObj = (Assassin)(object)enemy;
                    enemyObj.HP = enemyObj.HP - AttackDamage;
                }

            }
            else
            {
                Console.WriteLine("Oups... Not enough mana.");
            }
        }

        public override void SpecialAttack<T>(T enemy)
        {
            if ((Mana - 100) >= 0)
            {
                if (typeof(T) == typeof(Knight))
                {
                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {SpecialAttackDamage} dmg to {enemy}");
                    Mana = Mana - 100;

                    Knight enemyObj = (Knight)(object)enemy;
                    enemyObj.HP = enemyObj.HP - SpecialAttackDamage;
                }
                else if (typeof(T) == typeof(Defender))
                {
                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {SpecialAttackDamage} dmg to {enemy}");
                    Mana = Mana - 100;

                    Defender enemyObj = (Defender)(object)enemy;
                    enemyObj.HP = enemyObj.HP - SpecialAttackDamage;
                }
                else if (typeof(T) == typeof(Healer))
                {
                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {SpecialAttackDamage} dmg to {enemy}");
                    Mana = Mana - 100;

                    Healer enemyObj = (Healer)(object)enemy;
                    enemyObj.HP = enemyObj.HP - SpecialAttackDamage;
                }
                else if (typeof(T) == typeof(Battlemage))
                {
                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {SpecialAttackDamage} dmg to {enemy}");
                    Mana = Mana - 100;

                    Battlemage enemyObj = (Battlemage)(object)enemy;
                    enemyObj.HP = enemyObj.HP - SpecialAttackDamage;
                }
                else if (typeof(T) == typeof(Archer))
                {
                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {SpecialAttackDamage} dmg to {enemy}");
                    Mana = Mana - 100;

                    Archer enemyObj = (Archer)(object)enemy;
                    enemyObj.HP = enemyObj.HP - SpecialAttackDamage;
                }
                else if (typeof(T) == typeof(Assassin))
                {
                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {SpecialAttackDamage} dmg to {enemy}");
                    Mana = Mana - 100;

                    Assassin enemyObj = (Assassin)(object)enemy;
                    enemyObj.HP = enemyObj.HP - SpecialAttackDamage;
                }
            }
            else
            {
                Console.WriteLine("Oups... Not enough mana.");
            }
        }

        public string Stats()
        {
            return $"Max HP: {HP}\n" +
                $"Max Mana: {Mana}\n" +
                $"Armor Rating: {ArmorRating}\n" +
                $"Movement Speed: {MovementSpeed}\n" +
                $"Attack Damage: {AttackDamage}\n" +
                $"Special Attack Damage: {SpecialAttackDamage}";
        }

        public override string ToString()
        {
            return $"Character name: {Name}\n" +
                $"Race: {Race}\n" +
                $"Gender: {Gender}\n" +
                $"Class: {ClassName}, {SubclassName}\n" +
                $"{Stats()}";
        }
    }
}
