﻿using System;
using System.Text;

namespace RPGCharacterGeneratorLogic
{
    public abstract class Warrior : IActions
    {
        public string Name { get; set; }
        public RaceEnum Race { get; set; }
        public GenderEnum Gender { get; set; }
        public ClassEnum ClassName { get; set; }

        public Warrior(string name, RaceEnum race, GenderEnum gender)
        {
            Name = name;
            Race = race;
            Gender = gender;
            ClassName = ClassEnum.Warrior;
        }

        public abstract void Move();

        public abstract void Attack<T>(T enemy);

        public abstract void SpecialAttack<T>(T enemy);

    }
}
