﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorLogic
{
    public interface IActions
    {
        void Move();
        void Attack<T>(T enemy);
        void SpecialAttack<T>(T enemy);
    }
}
