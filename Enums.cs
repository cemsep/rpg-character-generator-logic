﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacterGeneratorLogic
{
   
    public enum GenderEnum {
        Male,
        Female
    }

    public enum ClassEnum
    {
        Warrior,
        Mage,
        Thief
    }

    public enum SubclassEnum
    {
        Knight,
        Defender,
        Healer,
        Battlemage,
        Archer,
        Assassin
    }

    public enum RaceEnum
    {
        Human,
        Dwarf,
        Elf,
        Orc
    }
    
}
