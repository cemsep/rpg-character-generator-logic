﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacterGeneratorLogic
{
    public class Assassin : Thief
    {
        public SubclassEnum SubclassName { get; set; }
        public double HP { get; set; }
        public int Energy { get; set; }
        public int ArmorRating { get; set; }
        public int MovementSpeed { get; set; }
        public int AttackDamage { get; set; }
        public int SpecialAttackDamage { get; set; }

        public Assassin(string name, RaceEnum race, GenderEnum gender)
            : base(name, race, gender)
        {
            SubclassName = SubclassEnum.Assassin;
            HP = 300;
            Energy = 100;
            ArmorRating = 10;
            MovementSpeed = 150;
            AttackDamage = 100;
            SpecialAttackDamage = 200;
        }

        public override void Move()
        {
            throw new NotImplementedException();
        }

        public override void Attack<T>(T enemy)
        {
            if ((Energy - 20) >= 0)
            {
                if (typeof(T) == typeof(Knight))
                {
                    Knight enemyObj = (Knight)(object)enemy;
                    double damageDealt = (AttackDamage - (AttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 20;

                    enemyObj.HP = enemyObj.HP - damageDealt;

                }
                else if (typeof(T) == typeof(Defender))
                {
                    Defender enemyObj = (Defender)(object)enemy;
                    double damageDealt = (AttackDamage - (AttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 20;

                    enemyObj.HP = enemyObj.HP - damageDealt;
                }
                else if (typeof(T) == typeof(Healer))
                {
                    Healer enemyObj = (Healer)(object)enemy;
                    double damageDealt = (AttackDamage - (AttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 20;

                    enemyObj.HP = enemyObj.HP - damageDealt;
                }
                else if (typeof(T) == typeof(Battlemage))
                {
                    Battlemage enemyObj = (Battlemage)(object)enemy;
                    double damageDealt = (AttackDamage - (AttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 20;

                    enemyObj.HP = enemyObj.HP - damageDealt;
                }
                else if (typeof(T) == typeof(Archer))
                {
                    Archer enemyObj = (Archer)(object)enemy;
                    double damageDealt = (AttackDamage - (AttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 20;

                    enemyObj.HP = enemyObj.HP - damageDealt;
                }
                else if (typeof(T) == typeof(Assassin))
                {
                    Assassin enemyObj = (Assassin)(object)enemy;
                    double damageDealt = (AttackDamage - (AttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a normal attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 20;

                    enemyObj.HP = enemyObj.HP - damageDealt;
                }

            }
            else
            {
                Console.WriteLine("Oups... Not enough energy.");
            }
        }

        public override void SpecialAttack<T>(T enemy)
        {
            if ((Energy - 100) >= 0)
            {
                if (typeof(T) == typeof(Knight))
                {
                    Knight enemyObj = (Knight)(object)enemy;
                    double damageDealt = (SpecialAttackDamage - (SpecialAttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 100;

                    enemyObj.HP = enemyObj.HP - damageDealt;

                }
                else if (typeof(T) == typeof(Defender))
                {
                    Defender enemyObj = (Defender)(object)enemy;
                    double damageDealt = (SpecialAttackDamage - (SpecialAttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 100;

                    enemyObj.HP = enemyObj.HP - damageDealt;

                }
                else if (typeof(T) == typeof(Healer))
                {
                    Healer enemyObj = (Healer)(object)enemy;
                    double damageDealt = (SpecialAttackDamage - (SpecialAttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 100;

                    enemyObj.HP = enemyObj.HP - damageDealt;
                }
                else if (typeof(T) == typeof(Battlemage))
                {
                    Battlemage enemyObj = (Battlemage)(object)enemy;
                    double damageDealt = (SpecialAttackDamage - (SpecialAttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 100;

                    enemyObj.HP = enemyObj.HP - damageDealt;
                }
                else if (typeof(T) == typeof(Archer))
                {
                    Archer enemyObj = (Archer)(object)enemy;
                    double damageDealt = (SpecialAttackDamage - (SpecialAttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 100;

                    enemyObj.HP = enemyObj.HP - damageDealt;
                }
                else if (typeof(T) == typeof(Assassin))
                {
                    Assassin enemyObj = (Assassin)(object)enemy;
                    double damageDealt = (SpecialAttackDamage - (SpecialAttackDamage * ((double)enemyObj.ArmorRating / 100)));

                    Console.WriteLine($"{SubclassName} doing a special attack! Dealing {damageDealt} dmg to {enemy}");
                    Energy = Energy - 100;

                    enemyObj.HP = enemyObj.HP - damageDealt;
                }
            }
            else
            {
                Console.WriteLine("Oups... Not enough energy.");
            }
        }

        public string Stats()
        {
            return $"Max HP: {HP}\n" +
                $"Max Energy: {Energy}\n" +
                $"Armor Rating: {ArmorRating}\n" +
                $"Movement Speed: {MovementSpeed}\n" +
                $"Attack Damage: {AttackDamage}\n" +
                $"Special Attack Damage: {SpecialAttackDamage}";
        }

        public override string ToString()
        {
            return $"Character name: {Name}\n" +
                $"Race: {Race}\n" +
                $"Gender: {Gender}\n" +
                $"Class: {ClassName}, {SubclassName}\n" +
                $"{Stats()}";
        }
    }
}
